import http.client
import urllib
from urllib import request


def fetch_data(url: str, data: bytes or None = None, headers: dict or None = None) -> bytes:
    """Fetches a resource with the given URL, data, and headers

    :param url: the URL of the page to access
    :type url: str
    :param data: additional data to send to the server (optional)
    :type data: bytes or None
    :param headers: headers to be sent to the server (optional)
    :type headers: dict or None
    :return: the page as a bytes (non-decoded) or str (decoded)
    :rtype: bytes
    """

    if data and headers:
        req_obj: urllib.request.Request = urllib.request.Request(url, data=data, headers=headers)
    elif data:
        req_obj: urllib.request.Request = urllib.request.Request(url, data=data)
    elif headers:
        req_obj: urllib.request.Request = urllib.request.Request(url, headers=headers)
    else:
        req_obj: urllib.request.Request = urllib.request.Request(url)

    resp: http.client.HTTPResponse
    with urllib.request.urlopen(req_obj) as resp:
        resp_data: bytes = resp.read()

    return resp_data
