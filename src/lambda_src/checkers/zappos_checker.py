import json
from checkers import utils

API_DOMAIN: str = 'zappos1.p.rapidapi.com'
API_ENDPOINT: str = '/products/detail?productId='
SOURCE_STR: str = 'zappos'
ZAPPOS_STR: str = 'Zappos'


def get_data(product: dict, api_key: str, debug_level: int = 0):
    """Fetches Zappos product information.

    :param product: a dict containing the ID for a product, its styles and sizes
    :type product: dict
    :param api_key: RapidAPI key for the Zappos API
    :type api_key: str
    :param debug_level: in dev mode, has print statements and includes debug data in returned dict (0=production, 1=development)
    :type debug_level: int
    :return: if a sale emails should be sent (bool), product status and information (dict)
    :rtype: (bool, dict)
    """

    api_url: str = 'https://' + API_DOMAIN + API_ENDPOINT
    base_product_url: str = 'https://www.zappos.com/'
    headers: dict = {
        'x-rapidapi-host': API_DOMAIN,
        'x-rapidapi-key': api_key
    }
    send_sale_email: bool = False
    current_product_data: dict = {
        'id': product['id'],
        'source': SOURCE_STR,
        'source_pretty': ZAPPOS_STR
    }
    if debug_level == 1:
        current_product_data['raw_data'] = product

    # TODO: consider adding a try block here and at the end of this method, put the except (potential error handling)
    resp_data: bytes = utils.fetch_data(api_url + product['id'], headers=headers)
    resp_json: dict = json.loads(resp_data)

    # Make sure call succeeded
    if resp_json.get('statusCode'):
        current_product_data['found'] = True
        current_product_data['status'] = '200'
        current_product_data['message'] = 'OK'
        # For some reason, it's 'statusCode' when 200, 'status' otherwise
        # And while 'statusCode' has string values, 'status' gives ints
        # Excellent API design...
        reported_styles: list = resp_json['product'][0]['styles']
        wanted_styles_id: list = list(map(lambda style: style['id'], product['styles']))
        current_product_data['brand_name'] = resp_json['product'][0]['brandName']
        current_product_data['product_name'] = resp_json['product'][0]['productName']
        current_product_data['full_name'] = current_product_data['brand_name'] + ' ' + current_product_data['product_name']
        current_product_data['styles'] = []

        if debug_level == 1:
            print('Item: ' + resp_json['product'][0]['brandName'] + ' ' + resp_json['product'][0]['productName'])
        wanted_style_id: str
        for wanted_style_id in wanted_styles_id:
            wanted_style: dict = list(filter(lambda style: style['id'] == wanted_style_id, product['styles']))[0]
            reported_style_ids: list = list(map(lambda style: style['colorId'], reported_styles))
            current_style: dict = {
                'id': wanted_style['id']
            }
            if debug_level == 1:
                current_style['raw_data'] = wanted_style

            if wanted_style_id in reported_style_ids:
                # get specific one that matches
                matching_style: dict = list(filter(lambda style: style['colorId'] == wanted_style_id, reported_styles))[0]
                # store data
                matched_color: str = matching_style['color']
                matched_current_price: str = matching_style['price']
                matched_normal_price: str = matching_style['originalPrice']
                matched_on_sale: bool = float(matching_style['price'][1:]) <= float(wanted_style['target_price'])
                current_style['found'] = True
                current_style['name'] = matched_color
                current_style['current_price'] = matched_current_price
                current_style['normal_price'] = matched_normal_price
                current_style['target_price'] = wanted_style['target_price']
                current_style['on_sale'] = matched_on_sale
                current_style['url'] = base_product_url + matching_style['productUrl']
                current_style['image_url'] = base_product_url + matching_style['imageUrl']
                if debug_level == 1:
                    print(' Color: ' + matched_color)
                    print('  Price: ' + matched_current_price)
                if matched_on_sale:
                    send_sale_email = True
                    if debug_level == 1:
                        print('  Below threshold! Normally: ' + matched_normal_price)
                # match sizes
                # Although there is the ['styles'][#]['stocks'] key which has
                # size, size name, and stock, if a size is out of stock it will
                # not show up there. In that case, you still have to check the
                # sizing data the long way to make sure they did not supply an
                # invalid size.
                matched_size_data_full: dict = resp_json['product'][0]['sizing']
                matched_size_info: list = list(filter(lambda size: size['id'] in wanted_style['target_sizes'], matched_size_data_full['allValues']))
                matched_sizes: list = list(map(lambda size: size['id'], matched_size_info))
                # determine stock for sizes
                wanted_size_stock_data: list = []
                size_id: str
                for size_id in wanted_style['target_sizes']:
                    size_found: bool = size_id in matched_sizes
                    if size_found:
                        size_info: dict = list(filter(lambda size: size['id'] == size_id, matched_size_info))[0]
                        # locate stock
                        stock_info: dict = list(filter(lambda stock: stock['color'] == wanted_style['id'] and stock['d3'] == size_id, matched_size_data_full['stockData']))[0]
                        wanted_size_stock_data += [{
                            'found': size_found,
                            'size_id': size_id,
                            'size_name': size_info['value'],
                            'on_hand': stock_info['onHand']
                        }]
                    else:
                        wanted_size_stock_data += [{
                            'found': size_found,
                            'size_id': size_id,
                            'size_name': '',
                            'on_hand': -1
                        }]
                # store style data
                current_style['stock_data'] = wanted_size_stock_data

                if debug_level == 1:
                    print('  Sizes:')
                    wss: dict
                    for wss in wanted_size_stock_data:
                        if wss['found']:
                            print('   ' + wss['size_id'] + ' (size ' + wss['size_name'] + ') - ' + wss['on_hand'] + ' in stock')
                        else:
                            print('   ' + wss['size_id'] + ' - not found')
            else:
                current_style['found'] = False
                current_style['name'] = ''
                current_style['current_price'] = ''
                current_style['normal_price'] = ''
                current_style['on_sale'] = None
                current_style['url'] = ''
                current_style['image_url'] = ''
                current_style['stock_data'] = None
                if debug_level == 1:
                    print(' Style not found: ' + wanted_style_id + ' Available styles: ' + str(reported_style_ids))
            current_product_data['styles'] += [current_style]
        return send_sale_email, current_product_data
    else:
        # For some reason, it's 'statusCode' when 200, 'status' otherwise
        # And while 'statusCode' has string values, 'status' gives ints
        # Excellent API design...
        current_product_data['found'] = False
        current_product_data['status'] = str(resp_json['status'])
        current_product_data['message'] = str(resp_json['message'])
        if debug_level == 1:
            print('Error fetching product: ' + product['id'] + ' (error: ' + str(resp_json['status']) + ' ' + resp_json['message'] + ')')
        return send_sale_email, current_product_data
