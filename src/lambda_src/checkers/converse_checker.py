import html
import json
import locale
import re
import urllib.error
import urllib.request
from checkers import utils

SOURCE_STR: str = 'converse'
CONVERSE_SOURCE: str = 'Converse'

# Regexes
# Note to future maintainers: check out the nps_checker file for some potential
# pitfalls when modifying these.
JSON_FINDER_REGEX: str = r'(?:var\s+utag_data\s+=\s)({(.*\s*)+?})(?:;)'
NOT_FOUND_FINDER_REGEX: str = r'Error 404'
QUANTITY_FINDER_REGEX: str = r'(?:<input.+?id\s*=\s*\"Quantity\".+?data-available\s*=\s*")(\d+)(?:")'
SIZE_FINDER_REGEX: str = r'(?:.+?selected\s?=\s?&quot;selected&quot;\s+>\s+)(.+?)(?:\s*<\/option>)'


def get_data(url: str, target_price: int or float, currency_locale: str = 'en_US.utf8', debug_level: int = 0) -> dict:
    """Gets the product data for a Converse URL.

    Returns:
        (normal) dict of the format: ::

            {
                'status': str ('200' if found, else the appropriate error code),
                'source': str (always 'converse'),
                'source_pretty': str (the human-readable name of the website used),
                'name': str (product name, including color),
                'price': str,
                'normal_price': str,
                'target_price': str,
                'on_sale': bool (True if below the normal_price),
                'below_threshold': bool (True if below the specified target_price),
                'color': str,
                'size': str,
                'sku': str,
                'in_stock': bool,
                'stock_left': int (how many are left),
                'url': str (the url you provided),
                'img_url': str (direct link to the link preview product image)
            }

        (error) dict of the format: ::

            {
                'status': str (error code),
                'message': str (error message),
                'source': str (always 'converse'),
                'source_pretty': str (the human-readable name of the website used),
                'url': str (the url you provided)
            }

    :param url: the url for the Converse website, including any relevant subdomain (required)
    :type url: str
    :param target_price: the price below which the application should send an alert (required)
    :type target_price: float or int
    :param currency_locale: the locale shortcode, used with currency formatting. Note that you MUST use an underscore (not a dash), capitalize the country code, and append ".utf8" or you might get "locale.Error: unsupported locale setting" (optional; see https://stackoverflow.com/a/3191729)
    :type currency_locale: str
    :param debug_level: in dev mode, has print statements and includes debug data in returned dict (0=production, 1=development, 2=verbose)
    :type debug_level: int
    :return: the product status and information
    :rtype: dict
    """

    data: dict = {
        'source': SOURCE_STR,
        'source_pretty': CONVERSE_SOURCE,
        'url': url
    }

    try:
        resp_data: str = utils.fetch_data(url).decode()
        # Converse returns 404s as 200s, even with no user-agent string, so we
        # have to parse the page for the error message
        if len(re.split(NOT_FOUND_FINDER_REGEX, resp_data)) > 1:
            if debug_level > 1:
                print('Error 404 occurred; returning')
            data['status'] = '404'
            data['message'] = 'Not found'
            return data
        data['status'] = '200'
    except urllib.error.HTTPError as err:
        if debug_level > 1:
            print('HTTPError occurred; returning')
        data['status'] = str(err.code)
        data['message'] = str(err.reason)
        return data

    if debug_level > 1:
        'Response data: ' + str(resp_data)

    # Parse page for data
    json_data: dict = json.loads(re.split(JSON_FINDER_REGEX, resp_data)[1])
    if debug_level > 0:
        print('Found json_data: ' + str(json_data))
    quantity: int = int(re.split(QUANTITY_FINDER_REGEX, resp_data)[1])
    if debug_level > 0:
        print('Found quantity: ' + str(quantity))
    in_stock: bool = quantity > 0
    size: str = html.unescape(re.split(SIZE_FINDER_REGEX, resp_data)[1])
    # Have to unescape it because otherwise it'll be something like "Women&#39;s 9"
    if debug_level > 0:
        print('Found size: ' + str(size))

    if json_data['product_brand'][0].capitalize() not in json_data['product_name'][0]:
        # Avoid duplicating the brand name
        # .capitalize() used because the embedded JSON's brand name is in lowercase
        data['name'] = json_data['product_brand'][0].capitalize() + ' ' + json_data['product_name'][0]
    else:
        data['name'] = json_data['product_name'][0]
    if debug_level > 0:
        print('Setting locale...')
    locale.setlocale(locale.LC_ALL, currency_locale)
    data['price'] = locale.currency(float(json_data['product_display_price'][0]))
    data['normal_price'] = locale.currency(float(json_data['product_msrp_price'][0]))
    data['target_price'] = locale.currency(target_price)
    data['on_sale'] = float(json_data['product_display_price'][0]) < float(json_data['product_msrp_price'][0])
    data['below_threshold'] = float(json_data['product_display_price'][0]) <= target_price
    data['color'] = json_data['product_color_family'][0]
    data['size'] = size
    data['in_stock'] = in_stock
    data['stock_left'] = quantity
    data['sku'] = json_data['product_sku_id'][0]
    data['img_url'] = json_data['product_image_url'][0]

    if debug_level > 0:
        data['raw_data'] = {
            'url': url,
            'target_price': target_price,
            'currency_locale': currency_locale
        }

    # Return data
    if debug_level > 1:
        print('Function complete; returning')
    return data
