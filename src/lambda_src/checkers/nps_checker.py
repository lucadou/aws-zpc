import json
import locale
import re
import urllib.error
import urllib.request
from checkers import utils

SOURCE_STR: str = 'nps'
NPS_SOURCE: str = 'NPS/Solovair'
SD_SOURCE: str = 'Solovair Direct'
# Regexes
# Note to future maintainers: when parsing HTML, it is generally a good idea to
# use (.+?) instead of (.+) when searching for HTML tag attributes, as using
# the latter could result in:
# <meta property="foo" content="foobar"><meta property="bar" content="foobar">
# matching:
# foobar"><meta property="bar" content="foobar
# instead of just:
# foobar
# due to how greedy quantifiers work. Adding the ? makes it non-greedy.
# https://stackoverflow.com/a/766377
# When testing individual strings, it is easy to fall into the trap of using the
# latter without thinking about how applications or middleware might one-line
# data, possibly inadvertently (if the developers are going for well-indented
# HTML) but also intentionally if the goal is to save bandwidth.
JSON_FINDER_REGEX: str = r'(?:var\smeta\s?=\s?)({.+?})(?:;)'
# Note that it is possible this could cause problems in josn.loads if it hit
# "var meta = { "stuff": "{};" };"
# but that is a case not really worth worrying about until it actually starts
# happening.
IMAGE_FINDER_REGEX: str = r'(?:<meta\s+property\s?=\s?\"og:image:secure_url\"\s+content\s?=\s?\")(.+?)(?:\"\s*>)'
PRICE_FINDER_REGEX: str = r'(?:<meta\s+property\s?=\s?\"og:price:amount\"\s+content\s?=\s?\")(.+?)(?:\"\s*>)'
STOCK_FINDER_REGEX: str = r'(?:<span\s+id\s?=\s?\"AddToCartText-product-template\">\s*)(Sold out)(?:\s*<\/span>)'
TITLE_FINDER_REGEX: str = r'(?:<meta\s+property\s?=\s?\"og:title\"\s+content\s?=\s?\")(.+?)(?:\"\s*>)'
VARIANT_FINDER_REGEX: str = r'(?:variant=)(\d+)'


def get_source_str(url: str) -> str:
    """Gets the source string for the given URL. Intended for use in error handling.

    :param url: the URL to inspect
    :type url: str
    :return: the appropriate website name string
    :rtype: str
    """
    if 'nps-solovair' in url:
        return NPS_SOURCE
    elif 'solovairdirect' in url:
        return SD_SOURCE
    else:
        return 'Unknown'


def get_data(url: str, target_price: int or float, normal_price: int or float, currency_locale: str = 'en_US.utf8', debug_level: int = 0) -> dict:
    """Gets the product data for a given NPS or Solovair URL.

    Works on nps-solovair.com and its subdomains.

    Returns:
        (normal) dict of the format: ::

            {
                'status': str ('200' if found, else the appropriate error code),
                'source': str (always 'nps'),
                'source_pretty': str (the human-readable name of the website used),
                'name': str (product name, including color),
                'price': str,
                'normal_price': str,
                'target_price': str,
                'on_sale': bool (True if below the normal_price),
                'below_threshold': bool (True if below the specified target_price),
                'size': str,
                'sku': str,
                'variant': int,
                'in_stock': bool,
                'url': str (the url you provided),
                'img_url': str (direct link to the link preview product image)
            }

        (error) dict of the format: ::

            {
                'status': str (error code),
                'message': str (error message),
                'source': str (always 'nps'),
                'source_pretty': str (the human-readable name of the website used),
                'url': str (the url you provided)
            }

    :param url: the url for the NPS-Solovair website, including any relevant subdomain (required)
    :type url: str
    :param target_price: the price below which the application should send an alert (required)
    :type target_price: float or int
    :param normal_price: the usual price for the product (required)
    :type normal_price: float or int
    :param currency_locale: the locale shortcode, used with currency formatting. Note that you MUST use an underscore (not a dash), capitalize the country code, and append ".utf8" or you might get "locale.Error: unsupported locale setting" (optional; see https://stackoverflow.com/a/3191729)
    :type currency_locale: str
    :param debug_level: in dev mode, has print statements and includes debug data in returned dict (0=production, 1=development, 2=verbose)
    :type debug_level: int
    :return: the product status and information
    :rtype: dict
    """
    data: dict = {}

    try:
        resp_data: str = utils.fetch_data(url).decode()
    except urllib.error.HTTPError as err:
        data: dict = {
            'status': str(err.code),
            'message': str(err.reason),
            'source': SOURCE_STR,
            'source_pretty': get_source_str(url),
            'url': url
        }
        return data

    if debug_level > 1:
        'Response data: ' + str(resp_data)

    # Parse page for data
    variant: int = int(re.split(VARIANT_FINDER_REGEX, url)[1])
    if debug_level > 0:
        print('Found variant: ' + str(variant))
    json_data: dict = json.loads(re.split(JSON_FINDER_REGEX, resp_data)[1])
    if debug_level > 0:
        print('Found json_data: ' + str(json_data))
    try:
        variant_details: dict = \
            list(filter(lambda curr_variant: curr_variant['id'] == variant, json_data['product']['variants']))[0]

        if debug_level > 0:
            print('Found variant_details: ' + str(variant_details))

        vendor: str = json_data['product']['vendor']
        name: str = re.split(TITLE_FINDER_REGEX, resp_data)[1]
        if vendor not in name:  # Prevent duplicating the collection name
            name: str = vendor + ' ' + name
        if debug_level > 0:
            print('Found name: ' + str(name))
        # I use regex on HTML instead of checking variant_details because variant_details has the size in the name

        price: float = float(re.split(PRICE_FINDER_REGEX, resp_data)[1])
        if debug_level > 0:
            print('Found price: ' + str(price))
        # I use regex on HTML instead of checking variant_details because variant_details has no decimal separator
        on_sale: bool = price < normal_price
        below_threshold: bool = price <= target_price
        if debug_level > 0:
            print('Target price: ' + str(target_price))
            print('On sale? ' + str(on_sale))
            print('Below threshold? ' + str(below_threshold))
        if debug_level > 0:
            print('Setting locale...')
        locale.setlocale(locale.LC_ALL, currency_locale)
        price: str = locale.currency(price)
        target_price: str = locale.currency(target_price)
        normal_price: str = locale.currency(normal_price)
        if debug_level > 0:
            print('Locale-formatted price: ' + str(price))
            print('Locale-formatted normal_price: ' + str(normal_price))
            print('Locale-formatted target_price: ' + str(target_price))

        size: str = variant_details['public_title']
        sku: str = variant_details['sku']
        in_stock: bool = len(re.split(STOCK_FINDER_REGEX, resp_data, flags=re.IGNORECASE)) == 1
        if debug_level > 0:
            print('In stock? ' + str(in_stock))
        # STOCK_FINDER_REGEX matches out of stock messages; if there is a match, the product is unavailable
        img_url: str = re.split(IMAGE_FINDER_REGEX, resp_data)[1]
        if debug_level > 0:
            print('Found image URL: ' + str(img_url))
        # There will typically be >1 match, but I have designed the regex with that in mind

        if debug_level > 1:
            print('Preparing return data...')

        data['status'] = '200'
        data['source'] = SOURCE_STR
        data['source_pretty'] = get_source_str(url)
        data['name'] = name
        data['price'] = price
        data['normal_price'] = normal_price
        data['target_price'] = target_price
        data['on_sale'] = on_sale
        data['below_threshold'] = below_threshold
        data['size'] = size
        data['sku'] = sku
        data['variant'] = variant
        data['in_stock'] = in_stock
        data['url'] = url
        data['img_url'] = img_url
    except IndexError:
        # This tends to happen when the variant changes. I do not know why the
        # variant changes, but I think it occurs whenever they introduce new
        # colors.
        # When that occurs, this would attempt to access [0] on an empty list,
        # causing this exception.
        # The solution is to update the URL in the watchlist data.
        print('Error: unable to locate variant {}'.format(str(variant)))
        print('Make sure the URL is correct/has not changed')
        data['status'] = '404'
        data['source'] = SOURCE_STR
        data['message'] = 'Not found'
        data['source_pretty'] = get_source_str(url)
        data['url'] = url

    if debug_level > 0:
        data['raw_data'] = {
            'url': url,
            'target_price': target_price,
            'normal_price': normal_price,
            'currency_locale': currency_locale
        }

    # Return data
    if debug_level > 1:
        print('Function complete; returning')
    return data
