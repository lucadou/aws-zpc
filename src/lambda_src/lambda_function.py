import base64
import boto3
import json
import os
from checkers import converse_checker
from checkers import nps_checker
from checkers import zappos_checker
# The 2 checker imports throw errors in PyCharm, but can be ignored. Basically,
# PyCharm wants you to use a relative import (e.g. "from .checkers import nps_checker"),
# but when this file runs, it will fail with ImportError (attempted relative
# import with no known parent package).
# I'm assuming this is a bug in PyCharm.
from datetime import datetime

WATCHLIST: dict = json.loads(base64.b64decode(os.environ['watchlist']).decode('utf-8'))
ZAPPOS_API_KEY: str = os.environ['zappos_api_key']
EMAIL_DELIMITER: str = os.environ['email_delimiter']
EMAIL_SUBJECT_SALE: str = os.environ['subject_sale']
EMAIL_SUBJECT_WEEKLY: str = os.environ['subject_weekly']
EMAIL_SENDER_ADDRESS: str = os.environ['email_sender']
# You can just give an email address (e.g. "noreply@example.com")
# or also supply a name (e.g. "No Reply <noreply@example.com>") and it will
# display as the sender name
EMAIL_RECIPIENT_ADDRESSES: list = os.environ['email_list'].split(EMAIL_DELIMITER)
# You can mix regular addresses and named addresses, e.g.
# "bobsmith@example.com|John Doe <jdoe@example.org>"
WEEKLY_EMAIL_DAY: int = int(os.environ['email_day'])  # Python weekday - Monday=0, Sunday=6
# https://docs.python.org/3/library/datetime.html#datetime.datetime.weekday
DEBUG_LEVEL: int = 0
# 0 = production
# 1 = testing locally; print stuff out
# The reason I do not read in the HTTP_PROXY environment variable set in the
# CloudFormation template is because:
# > In addition, if proxy settings are detected (for example, when a *_proxy
# > environment variable like http_proxy is set), ProxyHandler is default
# > installed and makes sure the requests are handled through the proxy.
# So I literally have to do nothing for proxy support.
# Source: https://docs.python.org/3/library/urllib.request.html#urllib.request.urlopen


def check_prices():
    """Checks price data on Converse, NPS, and Zappos.

    No parameters needed because this function pulls necessary data from the global variables above.

    Tuple return combinations:

    * If sale is False but message is a str, this is the weekly email.
    * If sale is True (and message is a str), this is a deal notification email.
    * If sale is False and message is None, there is no email to be sent.

    This function will never return sale True and message None.

    :return: a Tuple with 2 objects: sale (bool), message (str)
    :rtype: (bool, str)
    """

    all_product_data: list = []
    send_sale_email: bool = False
    watchlist: dict = WATCHLIST

    product: dict
    if zappos_checker.SOURCE_STR in watchlist:
        for product in watchlist[zappos_checker.SOURCE_STR]:
            email_status: bool
            product_data: dict
            email_status, product_data = zappos_checker.get_data(product, ZAPPOS_API_KEY, debug_level=DEBUG_LEVEL)
            all_product_data += [product_data]
            if email_status:
                send_sale_email = True

    if nps_checker.SOURCE_STR in watchlist:
        for product in watchlist[nps_checker.SOURCE_STR]:
            nps_data: dict
            if 'locale' in product:
                nps_data = nps_checker.get_data(product['url'], product['target_price'], product['normal_price'],
                                                currency_locale=product['locale'], debug_level=DEBUG_LEVEL)
            else:
                nps_data = nps_checker.get_data(product['url'], product['target_price'], product['normal_price'],
                                                debug_level=DEBUG_LEVEL)
            all_product_data += [nps_data]
            if nps_data['status'] == '200' and nps_data['below_threshold']:
                send_sale_email = True

    if converse_checker.SOURCE_STR in watchlist:
        for product in watchlist[converse_checker.SOURCE_STR]:
            converse_data: dict
            if 'locale' in product:
                converse_data = converse_checker.get_data(product['url'], product['target_price'],
                                                          currency_locale=product['locale'], debug_level=DEBUG_LEVEL)
            else:
                converse_data = converse_checker.get_data(product['url'], product['target_price'], debug_level=DEBUG_LEVEL)
            all_product_data += [converse_data]
            if converse_data['status'] == '200' and converse_data['below_threshold']:
                send_sale_email = True

    # Prepare email
    if DEBUG_LEVEL == 1:
        print(all_product_data)
        print('')

    css_img: str = 'max-width: 150px;'
    css_padding: str = 'padding: 1em;'
    error_products: list = []

    intro_sale: str = '<h3>Salutations! </h3>One or more of the items you are monitoring have gone on sale below their price threshold.'
    intro_weekly: str = '<h3>Greetings! </h3>This is your weekly email update to remind you this serice is active. You should recieve another one in 7 days.'
    # I include the space so a summary view of the email will have a space
    # in it; it is not rendered as HTML but it is does look better when in
    # a text summary.
    footer: str = '<i>This email was sent by an automated system. Please do not respond to this message.</i>'

    table_head: str = '<thead>'
    table_head += '<tr>'
    table_head += '<th colspan="2">Product </th>'
    table_head += '<th>Price </th>'
    table_head += '<th>In Stock? </th>'
    table_head += '<th>Link </th>'
    table_head += '</tr>'
    table_head += '</thead>'

    table_body: str = '<tbody>'
    for product in all_product_data:
        if product['status'] == '200':
            if product['source'] == zappos_checker.SOURCE_STR:
                for style in product['styles']:
                    p_name: str = product['full_name'] + ' (ID ' + product['id'] + ')'
                    if style['found']:
                        # c_ used to indicate color/style-related variables
                        c_name: str = style['name'] + ' (ID ' + style['id'] + ')'
                        p_name += '<br><i>Color: ' + c_name + '</i>'
                        c_price: str = style['current_price']
                        c_link: str = '<a href="' + style['url'] + '">Go</a>'
                        c_image: str = '<img src="' + style['image_url'] + '"'
                        c_image += ' style="' + css_img + '">'
                        if style['current_price'] != style['normal_price']:
                            # I do not use style['on_sale'], as that is only set
                            # for determining if an email should be sent immediately
                            c_price += '<br><i>(Usually ' + style['normal_price'] + '!)</i>'
                        size: dict
                        for size in style['stock_data']:
                            # s_ used to indicate style/stock-related variables
                            s_stock: str = size['on_hand']
                            if size['found']:
                                s_name = size['size_name'] + ' (ID ' + size['size_id'] + ')'
                                s_name = p_name + '<br><i>' + s_name + '</i>'
                                # Note the price threshold the user set, if below that threshold
                                if style['on_sale']:
                                    s_name += '<br><b>Sale alert triggered at: $' + str(style['target_price']) + '</b>'
                                if int(s_stock) > 0:
                                    s_stock = 'Yes (' + s_stock + ' left)'
                                else:
                                    s_stock = 'No'

                            else:
                                s_name = 'Error: Unable to find size (ID ' + size['size_id'] + ')'
                                s_name = p_name + '<br><i>' + s_name + '</i>'
                                s_stock = 'Unknown'
                            table_body += '<tr>'
                            table_body += '<td style="' + css_padding + '">' + c_image + '</td>'
                            table_body += '<td style="' + css_padding + '">' + s_name + '</td>'
                            table_body += '<td style="' + css_padding + '">' + c_price + '</td>'
                            table_body += '<td style="' + css_padding + '">' + s_stock + '</td>'
                            table_body += '<td style="' + css_padding + '">' + c_link + '</td>'
                            table_body += '</tr>'
                    else:
                        error_products += [{
                            'status': '200',
                            'source_pretty': product['source_pretty'],
                            'source': product['source'],
                            'id': product['id'],
                            'full_name': product['full_name'],
                            'style': style['id']
                        }]
            elif product['source'] == nps_checker.SOURCE_STR:
                p_img: str = '<img src="' + product['img_url'] + '"'
                p_img += ' style="' + css_img + '">'
                p_name: str = product['name'] + ' (SKU ' + product['sku'] + ')'
                p_name += '<br><i>Variant: ' + str(product['variant']) + '</i>'
                p_name += '<br><i>' + product['size'] + '</i>'
                if product['below_threshold']:
                    p_name += '<br><b>Sale alert triggered at: ' + product['target_price'] + '</b>'
                p_price: str = product['price']
                if product['on_sale']:
                    p_price += '<br><i>(Usually ' + product['normal_price'] + '!)</i>'
                p_stock = product['in_stock']
                if p_stock:
                    p_stock = 'Yes'
                else:
                    p_stock = 'No'
                p_link: str = '<a href="' + product['url'] + '">Go</a>'
                table_body += '<tr>'
                table_body += '<td style="' + css_padding + '">' + p_img + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_name + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_price + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_stock + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_link + '</td>'
                table_body += '</tr>'
            elif product['source'] == converse_checker.SOURCE_STR:
                p_img: str = '<img src="' + product['img_url'] + '"'
                p_img += ' style="' + css_img + '">'
                p_name: str = product['name'] + ' (SKU ' + product['sku'] + ')'
                p_name += '<br><i>Color: ' + str(product['color']) + '</i>'
                p_name += '<br><i>' + product['size'] + '</i>'
                if product['below_threshold']:
                    p_name += '<br><b>Sale alert triggered at: ' + product['target_price'] + '</b>'
                p_price: str = product['price']
                if product['on_sale']:
                    p_price += '<br><i>(Usually ' + product['normal_price'] + '!)</i>'
                p_in_stock: bool = product['in_stock']
                p_stock: str
                if p_in_stock:
                    p_stock = 'Yes (' + str(product['stock_left']) + ' left)'
                else:
                    p_stock = 'No'
                p_link: str = '<a href="' + product['url'] + '">Go</a>'
                table_body += '<tr>'
                table_body += '<td style="' + css_padding + '">' + p_img + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_name + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_price + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_stock + '</td>'
                table_body += '<td style="' + css_padding + '">' + p_link + '</td>'
                table_body += '</tr>'
        else:
            error_products += [product]
    table_body += '</tbody>'
    table: str = '<table>' + table_head + table_body + '</table>'

    error_list: str = ''
    if len(error_products) > 0:
        error_list += 'The following products could not be located:'
        error_list += '<ul>'
        error: dict
        for error in error_products:
            error_list += '<li>'
            if error['source'] == zappos_checker.SOURCE_STR:
                if error['status'] == '200':  # Product was found; style was not
                    error_list += error['source_pretty'] + ' ID "' + error['id'] + '" (' + error['full_name'] + ') - product was found but style ID "' + error['style'] + '" was not found'
                else:  # Product was not found
                    error_list += error['source_pretty'] + ' ID "' + error['id'] + '" - ' + error['status'] + ' ' + error['message']
            elif error['source'] in [nps_checker.SOURCE_STR, converse_checker.SOURCE_STR]:
                error_list += error['source_pretty'] + ' URL "' + error['url'] + '" - ' + error['status'] + ' ' + error['message']
            error_list += '</li>'
        error_list += '</ul>'
    message: str or None = '<br><br><br>' + table + error_list + footer

    if datetime.today().weekday() == WEEKLY_EMAIL_DAY:
        message = intro_weekly + message
        send_sale_email = False
    else:
        if send_sale_email:
            message = intro_sale + message
        else:
            message = None

    if DEBUG_LEVEL == 1:
        print(message)

    return send_sale_email, message


def lambda_handler(event, context):
    """The entry point for the Lambda.

    Note: the type of context is not disclosed.
    https://docs.amazonaws.cn/en_us/lambda/latest/dg/python-handler.html

    :param event: Event data
    :type event: dict or list or str or int or float or None
    :param context: Lambda runtime context
    """
    print('Checking prices...')
    sale_email: bool
    message: str
    sale_email, message = check_prices()
    if message:
        subject: str = ''
        print('Email data prepared')
        if sale_email:
            print('Sale below price threshold')
            subject = EMAIL_SUBJECT_SALE
        else:
            print('Weekly email')
            subject = EMAIL_SUBJECT_WEEKLY
        # Create SES client and send messages
        print('Sending emails...')
        ses_client: boto3.session.Session.client = boto3.client('ses')
        address: str
        for address in EMAIL_RECIPIENT_ADDRESSES:
            ses_response: dict = ses_client.send_email(
                Source=EMAIL_SENDER_ADDRESS,
                Destination={
                    'ToAddresses': [
                        address
                    ]
                },
                Message={
                    'Subject': {
                        'Data': subject
                    },
                    'Body': {
                        'Html': {
                            'Data': message
                        }
                    }
                }
            )
            print('Email address: ' + address)
            print('Response: ' + str(ses_response))
    else:
        print('Not publishing message - wrong day and no deals meet the thresholds set')
