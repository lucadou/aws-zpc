# AWS Lambda Price Checker

AWS Lambda function to pull price data from several websites and alert you
whenever they drop below a specified threshold via email. You can choose
specific styles and specify the size for good measure. It also sends an email
once a week to remind you what items it is monitoring and their current prices.

![A screenshot of an email showing a table of 2 pairs of shoes with images, their names, specified size and color, price, quantity in stock, and a link to buy them](img/email_screenshot_short.png)

Supported websites:
* Converse
* NPS/Solovair
* Solovair Direct
* Zappos

Requirements:

* An AWS account (services used: CloudFormation, EventBridge, IAM, Lambda, and SES)
* (If using the Zappos price checker:) A RapidAPI account
* (If using the Converse price checker:) A non-AWS HTTP proxy
* Python 3 with the pipenv package installed
* Know what you want to buy!

### Getting Started

This project makes use of AWS Lambda, IAM, and SES, and includes a
CloudFormation template file to speed deployment up. Unfortunately, part of the
SES setup cannot be automated. Specifically, the email and domain verification
portion is something that does not appear to be automatable via CloudFormation.

To learn how to verify an email address in SES, click
[here.](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses.html)  
To learn how to verify a domain in SES, click
[here.](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domains.html)

Once you have verified the email addresses and/or domains you will be sending
to/from (if you verify a domain you can send emails to/from any address on that
domain), you can proceed with the next portion of the setup: dependencies and
preparing the parameters file.

Note that these instructions assume you will be using the CloudFormation script.
If you want to set things up manually, you can study the CloudFormation script
and figure out how to do it on your own - writing out instructions for that is
pointless since the AWS GUI does change from time to time.

### Dependency Setup

```bash
sudo apt install awscli # if not already installed
aws configure # if you have not already configured AWS CLI with credentials
cd src/
cp params.json params.json.example
# Edit params.json, then:
# The following steps are only necessary if diagnosing errors:
pipenv install
pipenv shell
```

This was tested with Ubuntu 20.04 but should work easily on older versions of
Ubuntu and under WSL.

### The Parameters File

The [params file](src/params.json.example) file contains several variables
which you will have to set. These variables are:

* `EMAILARNS` - This is a comma-separated list of SES ARNs for your email
  addresses - the one you are sending from, and the ones you are sending to.
  The structure of an SES ARN is:  
    ```
    arn:aws:ses:REGION:ACCOUNT_NUMBER:identity/EMAIL_ADDRESS
    ```
    Where:
	* `REGION` - the AWS region you want to send from (e.g. `us-east-1`
	* `ACCOUNT_NUMBER` - your 12-digit AWS account number (if this is a personal
      account, anyway - I do not know what would happen if this was an
      organization account). You can find this by clicking on your account name
      in the top-right corner of the AWS console. There will be a 12-digit number
      next to "My Account", with dashes separating every 4 characters. You will
      type the 12-digit number (with no dashes) into the `ACCOUNT_NUMBER`
      section. For example, if your account number is `1111-2222-3333`, you
      would use `111122223333`.  
      ![Screenshot highlighting the account number in the AWS console](img/aws_account_number_screenshot.png)
	* `EMAIL_ADDRESS` - this is self explanatory. I do not know if there are any
      characters you need to escape, or the correct way of escaping them.  
	  So, if you were to send an email to/from the US-East-1 region from account
      \# `1234-2345-3456` with the email address `johndoe@contoso.com`, your ARN
      would be `arn:aws:ses:us-east-1:123423453456:identity/johndoe@contoso.com`.
      Although you can easily find the ARN of a verified email address by going
      to that verified email address in the SES console and copying the
      "Identity ARN" attribute value, if you have verified a domain but not the
      specific address you are sending to/from, you will not be able to go
      through this method and will have to manually construct the ARN.
* `EMAILDAY` - This Lambda also sends a weekly email to remind you what you are
  montoring, in addition to emails whenever prices fall below the thresholds you
  set. This is a number, from 0-6, indicating which day of the week you want
  your weekly email on, where 0=Monday and 6=Sunday. The reason for this
  numbering is because Python's datetime library starts the week on Mondays
  ([source](https://docs.python.org/3/library/datetime.html#datetime.datetime.weekday)).
* `EMAILLIST` - This is a pipe-separated email list. The only limit is what
  Python can handle (and your wallet, if you exceed the SES free tier). Do not
  insert a space before or after each pipe, as I use Python's split function on
  the pipe ("|") symbol, so those spaces will be included in the email addresses
  SES attempts to send to. If you actually have an email address with a pipe
  symbol (which is technically valid according to
  [RFC 5322](https://www.ietf.org/rfc/rfc5322.txt)) to send to, override the
  `EMAILDELIMITER` variable in the parameters file and you should be good to go.
  (I chose the pipe symbol because commas in email addresses seem more common
  than pipes.)
* `HTTPPROXY` - This is a standard
  [proxy string](https://docs.openshift.com/enterprise/3.2/install_config/http_proxies.html#configuring-hosts-for-proxies)
  (so it can be authenticated or open). If you plan on using Converse URLs with
  this, *you MUST specify a non-AWS HTTP proxy*, as Converse blocks AWS IP
  ranges. If you are not going to monitor the Converse website, *you MUST*
  *delete the variable*. Note that, if you specify a proxy, ALL requests will
  go through there, not just the Converse ones.
* `WATCHLIST` - This is a base-64 encoded version of the configuration data
  structure. I recommend preparing this as follows:
    ```python
	import base64
	# Let "CONFIG" be the variable our configuration is stored in
	print(base64.b64encode(json.dumps(CONFIG).encode()).decode())
	```
	I will talk about how to create the configuration variable in the next section.
* `ZAPPOSAPIKEY` - This is simply your RapidAPI Zappos API key.

There are a few variables that are not present in the example params file but
which you can override. These variables are:

* `EMAILDELIMITER` - This is the symbol used to separate email addresses in
  `EMAILLIST`. If you want to use a non-pipe delimiter, include this variable
  in the params file to override it.
* `ENTRYPOINT` - This is the entry-point for the Lambda, in the format
  `file_name.function_name`, where `file_name` is the name of the file without
  its extension and `function_name` is the name of the function without any
  parenthesis or parameters. Only override this if you are modifying the Lambda
  and know what you are doing.
* `SALECHECKINTERVAL` - This is the interval for sale-checks (and subsequently,
  email sending). If you want to check more or less frequently, update this.
  However, you should keep in mind that this may result in having to upgrade
  your RapidAPI plan and incurring extra AWS charges. In addition, if you were
  to set it to <24 hours, e.g. every 6 hours, this will likely result in >1
  weekly email, as it would trigger multiple times on the weekly email day.
  Likewise, lowering the frequency to a value that is not a multiple of 7 days
  could cause you to not get a weekly email some weeks (e.g. if you set the
  interval to every 2 days, you will only get a "weekly" email every 2 weeks).  
  If you are messaging people other than yourself, make sure they are ok with
  a higher email frequency or not always getting a weekly email. Keep in mind
  that if your emails start getting rejected due to being blocked by the
  recipient mailservers (e.g. marked as spam), this will affect your AWS SES
  reputation.  
  If you wish to set a custom interval, you can use a cron or rate expression
  ([documentation](https://docs.aws.amazon.com/eventbridge/latest/userguide/scheduled-events.html)).
  While cron expressions can set exactly when the Lambda will be triggered, a
  rate expression is far simpler. If you use the default of `rate(1 day)` and
  create the CloudFormation stack at 23:00 (11:00 PM), the Lambda will run
  every day at that time (though the first run will be at 23:00 the next day),
  but if you re-deploy at 17:30 (5:30 PM), the Lambda will run every day at that
  time, instead.  
* `ZIPS3BUCKET` - This is the bucket where the zipped file containing the Lambda
  is stored. If you wish to customize the function and deploy your own, scroll
  down to the [Build](#building) section below, and override this variable in
  the params file.
* `ZIPS3KEY` - This is the path to the deployment zip file inside of
  `ZIPS3BUCKET`. If you build your own, you will have to change this, as it is
  highly unlikely you will get the same path as the most recent build uploaded.  
  Note: If you wish to use the most recent build, do not override `ZIPS3BUCKET`
  or `ZIPS3KEY`. I will make sure to update the CloudFormation template
  whenever I increment the version number.

### Watchlist Configuration

Telling the Lambda what Zappos products to watch is actually fairly simple once
you understand the IDs you have to collect. This is a sample configuration:

```python
{
    'zappos': [
        {
            'id': '118955',
            'styles': [
                {
                    'id': '5177',
                    'target_price': 140,
                    'target_sizes': ['88463', '88464'],
                    'comment': 'Purple UK 7/US W 9 and UK 8/US W 10'
                }, {
                    'id': '777423',
                    'target_price': 140,
                    'target_sizes': ['88463', '88464'],
                    'comment': 'Cherry Red UK 7/US W 9 and UK 8/US W 10'
                }
            ],
            'comment': 'Dr Martens 1460W'
        },
        {
            'id': '8905480',
            'styles': [
                {
                    'id': '20370',
                    'target_price': 80,
                    'target_sizes': ['3109'],
                    'comment': 'Plaid US W 10'
                }
            ],
            'comment': 'Kamik Abigail'
        },
        {
            'id': '9271865',
            'styles': [
                {
                    'id': '820243',
                    'target_price': 160,
                    'target_sizes': ['88464'],
                    'comment': 'Burgundy UK 8/US W 10'
                }
            ],
            'comment': 'Dr Martens Leona'
        }
    ]
}
```

To better explain this, let's break down the first part of the first item:

```python
    {
        'id': '118955',
        'styles': [
            {
                'id': '5177',
                'target_price': 140,
                'target_sizes': ['88463', '88464'],
                'comment': 'Purple UK 7/US W 9 and UK 8/US W 10'
            }, {
                ...
            }
        ],
        'comment': 'Dr Martens 1460W'
    }
```

For reference, this item can be found
[here.](https://www.zappos.com/p/dr-martens-1460-w-purple-smooth-leather/product/118955/color/5177)
* Line 2 - This ID refers to the product ID. For example, in the URL above, the
  product ID would be 118955.
* Line 5 - This ID refers to the color/style ID. For example, in the URL above,
  the color ID would be 5177.
    * If you were on the default color
      (["Black Nappa Leather"](https://www.zappos.com/p/dr-martens-1460-w/product/118955)),
      you could determine the color ID by switching to another color, then
      switching back. (For example, if we switched to "Purple Smooth Leather"
      and then switched back to "Black Nappa Leather", the URL would now
      contain the color ID of 82.)
* Line 6 - The target price is the price threshold for sending an email outside
  the weekly email. For example, if this were set to "130", if the price was
  dropped to $130.00 or below, you would get an email even if it was not on your
  weekly email day.
  Note that this can be an `int` or a `float` (no currency markers), which means
  you can specify a decimal (e.g. "130.99"), though it will be subject to the
  limitations of representing floating point numbers on your platform.
  I live in the US so I do not know how well this works if you are in another
  country and do not use USD (I would not be surprised if prices could vary in
  other countries outside margins of error for currency conversion, for
  example).
  And unfortunately, API does not currently have support for other currencies.
* Line 7 - Target sizes is an array that contains the sizes you are
  monitoring for. If you do not want to determine the size, you can put any
  number. The email will still indicate when there is a sale, as (to my
  knowledge) Zappos does not do sales for specific sizes.
  However, you will not know if the size(s) you are looking for are in stock
  or not. To have stock data included in the email:
    * Go to the RapidAPI Zappos test page for `GET products/detail`
      ([here](https://rapidapi.com/apidojo/api/zappos1?endpoint=apiendpoint_22d46bbd-d169-4004-8d8e-d4e633903623)
	* Fill in the `productId` parameter and hit the "Test Endpoint" button.
	* Navigate to `product` > `0` > `sizing` > `allValues` in the "Body"
      section of the results pane.
	* Open each one until you find the size(s) you are looking for.
	* Take the `id`s for those sizes and put them into the list.  
	  ![A screenshot showing the location of the size ID via RapidAPI for "UK 8 (US Women's 10)" for product ID 118955 ("Dr. Martens 1460 W")](img/rapidapi_size_identification_screenshot.png)  
      Alternatively, you can find size IDs without polling the API.
      If you go to the product you want on Zappos and Inspect Element on the
      size dropdown menu (1 in the screenshot below), expand the element (2),
      and you will see the size IDs next to each size name (3).  
	  ![A screenshot showing the location of the size IDs via Inspect Element for product ID 118955 ("Dr. Martens 1460 W")](img/zappos_size_identification_screenshot.png)
* Lines 8 and 13 - These are comments. They are not used by the application.
  You can use them to write what IDs correspond to, write something else, or remove them entirely.

You can list multiple sizes for specific styles, and multiple styles for the
same product, with each style having a different target price if you so desire.
You can also list multiple products to monitor.

The NPS/Solovair section of the watchlist is easier to configure than the
Zappos section:

```python
{
    'nps': [
        {
            'url': 'https://us.nps-solovair.com/products/s8-551-cr-g?variant=34717342957640',
            'normal_price': 215,
            'target_price': 200,
            'locale': 'en_US.utf8',
            'comment': '8 Eye Derby, Cherry Red, UK 7/US W 9 - US site'
        },
        {
            'url': 'https://www.nps-solovair.com/collections/solovair/products/s8-551-ox-g?variant=32493331152976',
            'normal_price': 175,
            'target_price': 180,
            'locale': 'en_GB.utf8',
            'comment': '8 Eye Derby, Oxblood, UK 7.5/US W 9.5 - UK site'
        },
        {
            'url': 'https://www.solovairdirect.com/collections/classic/products/solovair-8-eye-derby-in-oxblood?variant=7429615429',
            'normal_price': 209.99,
            'target_price': 199.99,
            'comment': '8 Eye Derby, Oxblood, UK 7/US W 9 - Solovair Direct'
        }
    ]
}
```

To better explain this, let's break down the first item:

```python
        {
            'url': 'https://us.nps-solovair.com/products/s8-551-cr-g?variant=34717342957640',
            'normal_price': 215,
            'target_price': 200,
            'locale': 'en_US.utf8',
            'comment': '8 Eye Derby, Cherry Red, UK 7/US W 9 - US site'
        }
```

* Line 2 - This is just the NPS/Solovair/Solovair Direct URL.
    The reason you supply URLs instead of product IDs is because although
    Zappos price checking uses an API, NPS/Solovair price checking directly
    scrapes the website (since there is no API). Since there is no way for
    the application to know what subdomain(s) you would want to use, you have
    to supply the full URL.
* Line 3 - Normal price is the usual price of the item. The reason you have to
specify this is because there is no handy API that provides this information.
* Line 4 - Target price, like for Zappos, is the price threshold for sending an
email outside the weekly email.
* Line 5 - The locale code is optional *IF* you would be using `en_US`. If
you want any other locale, you will need to specify it. You can find a list of
locale codes [here](https://www.science.co.il/language/Locale-codes.php).  
    Once you have identified your locale code, you will need to modify it
    slightly. Specifically, you will need to replace the dash ('-') with an
    underscore ('_'), make the country code uppercase, and append '.utf8'.
    For example, if your locale code is `fr-ch`, you would specify the code
    `'locale': 'fr_CH.utf8'`.  
    Now, why exactly would you want to specify a locale code? Well, different
    regions display numbers and currency symbols in different ways. For example,
    residents of America would write "$8,142.73", while residents of France
    would write "8 142,73 €". Since the NPS/Solovair website sells outside the
    US (using different subdomains), I have provided the ability to supply the
    locale code.  
    The reason you cannot supply a locale code for Zappos price checking is
    because the API I am using does not support anything other than the US
    website. I wish that limitation did not exist, but here we are.
* Line 6 - This is a comment. It is not used by the application, so you can opt
to use it for whatever you want (such as writing down the color and size) or
remove it entirely.

The Converse section is similar to the NPS/Solovair section in its complexity:

```python
    'converse': [
        {
            'url': 'https://www.converse.com/shop/p/twisted-vacation-chuck-taylor-all-star-unisex-high-top-shoe/167928F_070.html?pid=167928MP&dwvar_167928MP_size=070&dwvar_167928MP_color=cerise%20pink%2Fgame%20royal%2Fwhite&dwvar_167928MP_width=standard&styleNo=167928F&pdp=true&cgid=womens-shoes',
            'target_price': 50,
            'locale': 'en_US.utf8',
            'comment': 'Twisted Vacation Chucks, US W 9'
        },
        { ... }
    ]
```

The breakdown is exactly the same as the NPS/Solovair section, but there is no
`normal_price` attribute because the Converse website embeds that in the page.

However, before you add Converse website URLs to your watchlist, make sure you
have an HTTP proxy setup outside the AWS IP range (as mentioned in the
[parameters file](#the-parameters-file) section for the `HTTPPROXY` variable).

Once you have created your JSON, load it into Python, convert it to a usable
object with the `json` library, and you are good to go! (Instructions for this
are available in the [parameters file](#the-parameters-file) section for the
`WATCHLIST` variable.)

### Building

If you modify the Lambda, you will have to build the deployment zip file.
To do this:

```bash
cd src
# Make your changes to lambda_function.py, then:
S3_BUCKET=my-s3-bucket # set the S3_BUCKET Bash variable to whatever bucket you will be uploading to
S3_PATH=my/path # if you want to store the zip file inside a sub-directory, specify that here
# If you have set an S3_PATH, run this command:
aws cloudformation package --template-file packaging_template.json --s3-prefix $S3_PATH --s3-bucket $S3_BUCKET
# Otherwise, run this command:
aws cloudformation package --template-file packaging_template.json --s3-bucket $S3_BUCKET
```

Note: When modifying the Python file, make sure to not rename the
`lambda_handler` function or the `lambda_function.py` file itself unless you
intend to also override the `ENTRYPOINT` variable in your parameters file.

### Deployment

When deploying for the first time:

```bash
# Choose a stack name and store it in a Bash variable
STACK_NAME=my_stack_or_something
# Deploy the CloudFormation template
aws cloudformation deploy --stack-name $STACK_NAME --template-file cloudformation_template.json --parameter-overrides "file://`pwd`/params.json" --capabilities CAPABILITY_NAMED_IAM
```

### Updating

When updating the stack later on:

```bash
STACK_NAME=my_stack_or_something
aws cloudformation deploy --stack-name $STACK_NAME --template-file cloudformation_template.json --parameter-overrides "file://`pwd`/params.json" --capabilities CAPABILITY_NAMED_IAM
```

(This is the same command used in the [deployment section](#deployment).)

## Notes/FAQ

> Why did you make this?

If you like to wait for sales, there's nothing more frustrating than checking
Zappos every day. I got a pair of Doc Martens 40% off, but that deal was gone
the next day, so if I missed a day, I would have totally missed the deal!
Hence, the idea for this.

I also wanted to become more familiar with AWS Lambda, and this was a great
opportunity to do that. I could have setup a cron job on my EC2 instance that
used SES, but this is much more self-contained since it uses CloudFormation.

> How much will this cost?

Note: All prices are in USD and current on 2020-09-26.

As long as you do not exceed 500 RapidAPI API calls per month (which would
require monitoring over 15 items at any given time), that should cost you
nothing. If you want to monitor more than 15 items, you will probably need to
pay for RapidAPI's "Pro" plan ($10/month).

For AWS Lambda, the cost is $0.0000002083 per 100ms (assuming 128 MB RAM).
In my experience, 10 items on 128 MB RAM takes <7 seconds, so maybe
$0.000002083 per invocation, or $0.00006249 per month (assuming 30 invocations
that take 10 seconds each).
Lambda is also $0.20 per 1 million requests, but it is unclear if that is $0.20
for 1-1,000,000 or if that is $0.20 after the first million. Either way, it
should be under $0.25/month for Lambda usage.  
AWS EventBridge (the thing that causes the Lambda to trigger every day) costs
$1.00/million custom events (with an average of 30 events/month).   
AWS SES, by contrast, has a free-tier up to 62,000 messages per month. Unless
you have over 2,066 recipients (assuming 30 invocations per month), you will
not exceed the free tier.  
AWS CloudFormation is free for 1,000 handler operations per month, with
$0.0009 per handler thereafter, and $0.00008/second after the first 30 seconds.
In my experience, deploying this stack tends to take just under 2 minutes (as
of 2020-10-07 in US-East-1) and updates are <5 seconds, but to account for
possible future additions, I will assume all deployments and updates take 3
minutes.  
Even if you deploy or update once per day, manage 10 resources, take 2
operations per resource, take 90 seconds per operation, and deploy once a day,
your bill will be $2.92/month.

So in total, you would have to really be going nuts with this to have a bill
over $2.00/month. Chances are it will cost less than $0.10 per deployment and
only $1.00/month.

> How can I test the Lambda without waiting until 24 hours after I deploy the
stack?

Go to the Lambda in the AWS console, click on the "Test" button (in the row
below your function name), and it will run the function.  
If you have not yet created a test event, you will be prompted to do so. You
can use the "Hello World" test template, leave "Event name" blank, and type
`{}` for the JSON test data section, then hit the "Save" button. Then click
the "Test" button and the function will run.

Note: The email will only fire if it is on your weekly email day OR there is a
sale that goes below your price threshold. I recommend finding an item that is
on sale, loading it into your watchlist with a threshold above the current sale
price, and re-testing the Lambda. After confirming emails deliver, you can undo
the changes to your watchlist.

> Whenever I try to deploy/update, I get a ValidationError about the "HTTPPROXY"
environment variable. How do I fix this?

If you get `Parameters: [HTTPPROXY] do not exist in the template`, that means
you left `HTTPPROXY` blank. If you are not using a proxy, you must delete the
variable. Just leaving it blank in a parameter overrides file will not work.

> I have a sale that is below a price threshold but I never get any emails, even
on the weekly email day. The SES dashboard is showing 100% deliveries, 0
bounces, and 0 rejects. What's going on?

There are a few possibilities. The first is that the email got flagged as spam.
The second is that you are using SES incorrectly. The solution to the first is
simple - open your spam folder, mark it as not spam, and you should not have
any more problems.

The second possibility is more complex. When an email is rejected due to DMARC
settings, the SES mailer daemon will attempt to email you about the failure.
This would indicate SES must know that there is a rejection, but it never
actually displays this failure in the dashboard for whatever reason. However,
you may never get this email, and I will explain why after this screenshot of
what the email looks like:

![A screenshot of an email from the AWS SES mailer daemon indicating a message failed to deliver](img/email_screenshot_fail_dmarc.png)

So why would you not get this email? Well, the reason I got this email was
because the sending email address was an email on a domain with GSuite handling
email, and that address happened to be registered as an alias of my account.
The precise series of events that took place here are:

1. SES attempts to send an email from `noreply@example.org`.
2. Gmail servers receive an SES-signed email claiming to be from `example.org`.
3. Gmail checks the SPF record to locate the DKIM key stored in DNS. It is a
GSuite email key, not SES. Gmail is unable to verify the message signature.
4. Gmail then checks the DMARC record. It is set to
`v=DMARC1; p=reject; pct=100; sp=reject`, meaning all failures must be
rejected.
5. Gmail rejects the email.
6. SES receives the rejection and emails `noreply@example.org` informing the
address of the failure.
7. Because `noreply@example.org` was an alias of my account (`me@example.org`),
I receive the delivery failure notification.

If the email address you are sending from does not receive mail (or it does but
you do not have access to it), you will never be informed of the rejection.

The solution, in my case, was realizing that A) these SES delivery failure
notifications were being marked as spam by Gmail, and then that B) I meant to
email from `noreply@apps.example.org`, as `apps.example.org` was setup with SES
keys in the DNS DKIM entry and SPF set to reference the SES keys, with DMARC
configured securely (`v=DMARC1; p=reject; pct=100; sp=reject`).

So if DKIM failures occur, you are probably sending from a domain not verified
by SES (you can send to/from an email address verified by SES, but that does
not mean it will work). I recommend using the SES DKIM generator when verifying
domains unless you already have your own keys.

If you have setup DKIM correctly and are emailing from an SES verified domain,
I have not run into any errors like that so I do not have any guidance for how
to proceed.

> Converse URLs are showing up as 403 Forbidden, how do I fix this?

Converse blocks AWS IP ranges, so you have to supply a proxy server that is not
in AWS. If you have supplied a proxy, chances are it is blocked as well.

> What proxy did you use for testing?

I set a proxy up with Squid on my Raspberry Pi. I have its firewall setup to
only allow AWS US-East-1 IPs to access my Squid port, and only during a 10
minute interval (to give the script time in case it starts a bit early or late).

You can find the current AWS IP ranges [here](https://ip-ranges.amazonaws.com/ip-ranges.json)
(from [their documentation](https://docs.aws.amazon.com/general/latest/gr/aws-ip-ranges.html)).  
Documentation on the UFW commands I use can be found
[here](https://help.ubuntu.com/community/UFW#Allow_Access).  
There are a *lot* of Squid setup guides, so I will leave finding one as an
exercise for the reader (plus you could always use something besides Squid -
off the top of my head there's mitmproxy, Tinyproxy, Privoxy, WinGate (for
Windows), and you can even use Nginx as a forward proxy if you really want).

You could pay for a proxy, but I do not know which proxies are blocked by
Converse and which are not. You might be able to set one up in Google Cloud or
Azure (I have not tried).

> Zappos items list the color/style but NPS/Solovair items do not. Why is that?

Zappos price checking uses a handy API which includes color data, while
NPS/Solovair scrapes the website directly. There is no good way of scraping
the color name and subtracting it out from the product name, as sometimes
the full color name is not included consistently in the product name. It is what
it is.

> Is there a limit to how many items this can handle?

The only limits are your those imposed by your wallet (and Python's ability
to handle large amounts of data).

> Could you provide another example of what these emails look like that
emphasize any un-advertised features?

This is an email with a fair amount of test data that does a good job showing
how it handles corner cases like bad product, style, and size IDs, and out of
stock items:

![A screenshot of an email showing a table of 8 pairs of shoes with images, their names, specified size and color, price, quantity in stock, and a link to buy them, complete with edge cases](img/email_screenshot_full.png)

> What is the watchlist you use for testing?

The base64-encoded watchlist is (as of 2020-10-11):

```
eyJ6YXBwb3MiOiBbeyJpZCI6ICIxMTg5NTUiLCAic3R5bGVzIjogW3siaWQiOiAiNTE3NyIsICJ0YXJnZXRfcHJpY2UiOiAxNDAsICJ0YXJnZXRfc2l6ZXMiOiBbIjg4NDYzIiwgIjg4NDY0Il0sICJjb21tZW50IjogIlB1cnBsZSBTbW9vdGggTGVhdGhlciBVSyA3L1VTIFcgOSArIFVLIDgvVVMgVyAxMCJ9LCB7ImlkIjogIjc3NzQyMyIsICJ0YXJnZXRfcHJpY2UiOiAxNDAsICJ0YXJnZXRfc2l6ZXMiOiBbIjg4NDYzIiwgIjg4NDY0Il0sICJjb21tZW50IjogIkNoZXJyeSBSZWQgQXJjYWRpYSBMZWF0aGVyIFVLIDcvVVMgVyA5ICsgVUsgOC9VUyBXIDEwIn1dLCAiY29tbWVudCI6ICJEciBNYXJ0ZW5zIDE0NjBXIn0sIHsiaWQiOiAiODkwNTQ4MCIsICJzdHlsZXMiOiBbeyJpZCI6ICIyMDM3MCIsICJ0YXJnZXRfcHJpY2UiOiA4MCwgInRhcmdldF9zaXplcyI6IFsiMzEwOSJdLCAiY29tbWVudCI6ICJQbGFpZCBVUyBXIDEwIn1dLCAiY29tbWVudCI6ICJLYW1payBBYmlnYWlsIn0sIHsiaWQiOiAiOTI3MTg2NSIsICJzdHlsZXMiOiBbeyJpZCI6ICI4MjAyNDMiLCAidGFyZ2V0X3ByaWNlIjogMTYwLCAidGFyZ2V0X3NpemVzIjogWyI4ODQ2MSIsICI4ODQ2NCIsICIxMzM3Il0sICJjb21tZW50IjogIkJ1cmd1bmR5IFZpbnRhZ2UgVUsgNS9VUyBXIDcgKyBVSyA4L1VTIFcgMTAgKyBpbnZhbGlkIHNpemUgZm9yIHRlc3RpbmcifSwgeyJpZCI6ICIzIiwgInRhcmdldF9wcmljZSI6IDE2MCwgInRhcmdldF9zaXplcyI6IFsiMTMzNyJdLCAiY29tbWVudCI6ICJGb3IgdGVzdGluZyBpbnZhbGlkIHN0eWxlcyJ9XSwgImNvbW1lbnQiOiAiRHIgTWFydGVucyBMZW9uYSAtIGN1cnJlbnQgc2FsZSB0ZXN0aW5nIGl0ZW0gKG9uIHNhbGUgb24gMjAyMC0wOS0yNykifSwgeyJpZCI6ICIwIiwgImNvbW1lbnQiOiAiRm9yIHRlc3RpbmcgNDA0cyJ9LCB7ImlkIjogIm9uZSIsICJjb21tZW50IjogIkZvciB0ZXN0aW5nIGludmFsaWQgcHJvZHVjdCBJRHMifV0sICJucHMiOiBbeyJ1cmwiOiAiaHR0cHM6Ly91cy5ucHMtc29sb3ZhaXIuY29tL3Byb2R1Y3RzL3M4LTU1MS1jci1nP3ZhcmlhbnQ9MzQ3MTczNDI5NTc2NDAiLCAibm9ybWFsX3ByaWNlIjogMjE1LCAidGFyZ2V0X3ByaWNlIjogMjAwLCAiY29tbWVudCI6ICI4IEV5ZSBEZXJieSwgQ2hlcnJ5IFJlZCwgVUsgNy9VUyBXIDkgLSBVUyBzaXRlIn0sIHsidXJsIjogImh0dHBzOi8vd3d3LnNvbG92YWlyZGlyZWN0LmNvbS9jb2xsZWN0aW9ucy9jbGFzc2ljL3Byb2R1Y3RzL3NvbG92YWlyLTgtZXllLWRlcmJ5LWluLW94Ymxvb2Q/dmFyaWFudD03NDI5NjE1NDI5IiwgIm5vcm1hbF9wcmljZSI6IDIwOS45OSwgInRhcmdldF9wcmljZSI6IDIwMCwgImxvY2FsZSI6ICJlbl9VUy51dGY4IiwgImNvbW1lbnQiOiAiOCBFeWUgRGVyYnksIE94Ymxvb2QsIFVLIDcvVVMgVyA5IC0gU29sb3ZhaXIgRGlyZWN0In0sIHsidXJsIjogImh0dHBzOi8vd3d3Lm5wcy1zb2xvdmFpci5jb20vY29sbGVjdGlvbnMvc29sb3ZhaXIvcHJvZHVjdHMvczgtNTUxLW94LWc/dmFyaWFudD0zMjQ5MzMzMTE1Mjk3NiIsICJub3JtYWxfcHJpY2UiOiAxNzUsICJ0YXJnZXRfcHJpY2UiOiAxODAsICJsb2NhbGUiOiAiZW5fR0IudXRmOCIsICJjb21tZW50IjogIjggRXllIERlcmJ5LCBPeGJsb29kLCBVSyA3LjUvVVMgVyA5LjUgLSBVSyBzaXRlIn0sIHsidXJsIjogImh0dHBzOi8vd3d3Lm5wcy1zb2xvdmFpci5jb20vY29sbGVjdGlvbnMvc29sb3ZhaXIvcHJvZHVjdHMvbm90YXByb2R1Y3Q/dmFyaWFudD0xOTc3ODg4MTUxOTY4MCIsICJub3JtYWxfcHJpY2UiOiAxMDAsICJ0YXJnZXRfcHJpY2UiOiAiNzUiLCAiY29tbWVudCI6ICJOb24tZXhpc3RlbnQgTlBTL1NvbG92YWlyIHByb2R1Y3QgdG8gdGVzdCA0MDRzIn0sIHsidXJsIjogImh0dHBzOi8vd3d3LnNvbG92YWlyZGlyZWN0LmNvbS9jb2xsZWN0aW9ucy9jbGFzc2ljL3Byb2R1Y3RzL25vdGFwcm9kdWN0P3ZhcmlhbnQ9NzQyOTYxNTQyOSIsICJub3JtYWxfcHJpY2UiOiAxMDAsICJ0YXJnZXRfcHJpY2UiOiAiNzUiLCAiY29tbWVudCI6ICJOb24tZXhpc3RlbnQgU29sb3ZhaXIgRGlyZWN0IHByb2R1Y3QgdG8gdGVzdCA0MDRzIn1dfQ==
```

You can view the JSON contents by running (in Python):

```python
import base64
import json
import pprint
TEST_DATA = 'the base64 encoded data'
pprint.pprint(json.loads(base64.b64decode(TEST_DATA).decode()))
```

However, in the future, I am sure that the Leonas will no longer be on sale and
I will have to update the sale target. I will try to remember to update this
section when I do use different test data.

In the event that I update the test data, I do not plan on updating the
screenshots unless the format of the email changes.

> This system was working just fine but now some items are listed as 404, but
> whenever I go to the page they are clearly still there. Why is this?

I have noticed this on the NPS/Solovair website. It seems that whenever they
introduce new colors, they generate new variants, but the base URL still stays
the same. For example,
`https://us.nps-solovair.com/products/s8-551-cr-g?variant=34717342957640`, the
"Cherry Red 8 Eye Derby Boot" in UK 7/US W 9 had its URL changed to
`https://us.nps-solovair.com/products/s8-551-cr-g?variant=36508711157832` in
January 2021. However, if you go to the old link, the product is still there,
but it defaults to UK 3 / US W 5 because that variant of the product no longer
exists.
There could be other reasons URLs change, but that is the only one I have seen
so far.

## Versioning

This project uses Semantic Versioning, aka [SemVer](https://semver.org/spec/v2.0.0.html).

## Authors

* Luna Lucadou - Wrote the application in her free time

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.

## Bugs, feature ideas, etc?

Feel free to fill out an issue or submit a merge request!

