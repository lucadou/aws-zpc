# Changelog
All notable changes to this project will be documented in this file.

## [1.4.3] - 2021-04-01
### Changed
- Improved readme formatting
- Not found variants no longer crash the NPS/Solovair checker
- Re-packaged deployment file (file contents have changed)
- Updated readme with an explanation on how to deal with partially-404'd URLs

## [1.4.2] - 2020-12-22
### Changed
- Only using cloudformation deploy command in readme instead of deploy + update-stack (#6)

### Removed
- params-update file because it is not needed with only using deploy command

## [1.4.1] - 2020-12-22
### Changed
- Added Converse to list of supported websites in readme
- Changed code block language in readme
- Updated readme SES diagnostics to account for SES failures being marked as spam

## [1.4.0] - 2020-10-11
### Added
- Converse website checker (#2)
- Proxy support

### Changed
- NPS regex now matches embedded JSON with non-greedy quantifiers
- Re-packaged deployment file (file contents have changed)
- Updated AWS services used in readme
- Updated images in readme
- URL fetching now handled by a separate utilities file

## [1.3.1] - 2020-10-08
### Changed
- Product errors sections now differentiates between NPS/Solovair and Solovair Direct (#3)
- Re-packaged deployment file (file contents have changed)
- Reduced refactored source string comparisons to use constants

## [1.3.0] - 2020-10-07
### Added
- Collection name to product name, if it was not already present
- PEP8 compliance
- Solovair Direct support to readme
- Support for NPS/Solovair website
- Type annotations (https://docs.python.org/3/library/typing.html)

### Changed
- Moved Zappos price checking to a separate file
- Re-packaged deployment file (file contents have changed)
- Updated cost section of readme to differentiate between deploy vs update cost
- Updated screenshots in readme to show NPS/Solovair capabilities
- Updated test data to include Solovair Direct URLs
- Updated watchlist information in readme

## [1.2.4] - 2020-09-27
### Changed
- Many small corrections to the readme
- Updated order of requirements in readme

## [1.2.3] - 2020-09-27
### Added
- Current test data to readme
- Size ID finding method via inspect element to readme

### Changed
- Changed valid product with invalid style to append a message in the not found section (rather than have no image, price, or stock)
- Re-packaged deployment file (file contents have changed)
- Updated screenshots in readme to show new valid product/invalid style behavior

## [1.2.2] - 2020-09-26
### Added
- Separate IAM policy for SES permissions
- CloudWatch logs permission

### Changed
- Re-packaged deployment file (though the contents should be the same)
- Updated FAQ in readme

### Removed
- Inlined IAM policy for SES permissions

## [1.2.1] - 2020-09-26
### Changed
- Spacing changes in readme
- Wording changes in readme

### Removed
- Unused deployment zip file

## [1.2.0] - 2020-09-25
### Added
- CloudEvents timer rule
- More variables to CloudFormation template
- Build file
- Deployment and update instructions to readme

### Changed
- Redid instructions for changing zip file in readme
- Changed full-length email screenshot
- Upgraded runtime from Python 3.7 to Python 3.8

## [1.1.0] - 2020-09-24
### Added
- CloudFormation deployment template
- Config file

### Changed
- Overhauled readme with lots of instructions
- Changed lambda function to receive email list delimiter from environment variable

## [1.0.1] - 2020-09-23
### Changed
- Watchlist is now given as a base64-encoded string instead of a JSON-string

## [1.0.0] - 2020-09-23
### Added
- Initial release of the Lambda function
- Changelog
- License file
- Readme
